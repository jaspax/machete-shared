const _ = require('lodash');
const store = require('idb-keyval');

let memoryCache = {};
let ga = null;

function mkkey(name, args) {
    if (!name || name == 'undefined') {
        throw new Error('invalid cache name:', name);
    }
    return `${name}:${JSON.stringify(args)}`;
}

function val(value, timestamp = new Date()) {
    return { timestamp, value };
}

function runGetter(cb, getter, args, opts) {
    const key = mkkey(opts.name, args);
    if (!cb.pending[key]) {
        cb.pending[key] = (async() => {
            try {
                const rv = await getter(...args);
                await module.exports.setCache(key, rv);
                return rv;
            }
            finally {
                delete cb.pending[key];
            }
        })();
    }
    return cb.pending[key];
}

async function refresh(cb, getter, handler, args, opts) {
    const key = mkkey(opts.name, args);
    const { value, isStale } = await module.exports.cachedValue(key, opts);

    // Immediately call the handler with the cached value
    if (handler)
        await handler(value);

    if (isStale) {
        const newValue = await runGetter(cb, getter, args, opts);
        if (handler && !_.isEqual(value, newValue)) {
            await handler(newValue);
        }
        return newValue;
    }
    return value;
}

async function clear(name, ...args) {
    let filterFn = () => true;
    if (args.length) {
        const arg0 = args[0];
        if (arg0 && arg0.call) {
            filterFn = argStr => arg0(...JSON.parse(argStr));
        }
        else {
            const str = JSON.stringify(args);
            filterFn = argStr => argStr == str;
        }
    }

    const toDelete = [];
    for (const key of await store.keys()) {
        const firstColon = key.indexOf(':');
        const nameStr = key.substring(0, firstColon);
        const argStr = key.substring(firstColon + 1);

        if (nameStr == name) {
            if (filterFn(argStr)) {
                toDelete.push(key);
            }
        }
    }

    for (const key of toDelete) {
        delete memoryCache[key];
        await store.del(key);
    }
}

module.exports = {
    setAnalytics(_ga) {
        ga = _ga;
    },

    cacheable(getter, opts) {
        const cb = (...args) => refresh(cb, getter, null, args, opts);
        cb.refresh = (handler, ...args) => refresh(cb, getter, handler, args, opts);
        cb.force = (...args) => runGetter(cb, getter, args, opts);
        cb.clear = (...args) => clear(opts.name, ...args);
        cb.pending = {};

        return cb;
    },

    async clearCache() {
        memoryCache = {};
        await store.clear();
    },

    setCache(key, value) {
        const valueObj = val(value);
        memoryCache[key] = valueObj;
        store.set(key, valueObj).catch(ex => console.warn("Couldn't write to key storage:", ex));
    },

    async cachedValue(key, opts) {
        const now = Date.now();
        let stored = null;
        let isStale = true;
        let cacheLoc = null;
        if (stored = memoryCache[key]) { // eslint-disable-line no-cond-assign
            cacheLoc = 'mem';
            isStale = now - (opts.expireHours * 60 * 60 * 1000) > stored.timestamp.getTime();
        }
        else if (stored = await store.get(key)) { // eslint-disable-line no-cond-assign
            cacheLoc = 'store';
            isStale = now - (opts.expireHours * 60 * 60 * 1000) > stored.timestamp.getTime();
        }
        else {
            cacheLoc = 'miss';
            stored = val(opts.defaultValue, new Date(0));
        }
        if (ga)
            ga.mevent('cache', 'cached-value', `${cacheLoc}-${isStale ? 'stale' : 'fresh'}`);

        return {
            value: stored.value,
            timestamp: stored.timestamp,
            isStale,
        };
    },

    cacheSet(key, value) {
        return store.set(key, value);
    },

    cacheGet(key) {
        return store.get(key);
    },
};
