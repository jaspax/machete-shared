const round = {
    none: x => x,
    whole: Math.round,
    money: x => Math.round(x * 100) / 100
};

function moneyFmt(val) {
    const minus = val < 0 ? '-' : '';
    return minus + '$' + numberFmt(Math.abs(val), 2);
}

function pctFmt(val) {
    return numberFmt(val, 1) + '%';
}

function numberFmt(val, digits = 0) {
    val = Number(val);
    if (Number.isNaN(val)) {
        return ' -- ';
    }
    let start = val.toFixed(digits); 
    while (true) { // eslint-disable-line no-constant-condition
        const next = start.replace(/(\d)(\d\d\d([,.]|$))/, "$1,$2");
        if (start == next)
            return next;
        start = next;
    }
}

function roundFmt(val) {
    val = Number(val);
    if (Number.isNaN(val)) {
        return ' -- ';
    }
    return Math.round(val);
}

/* These properties are used to map together necessary properties with
 * metric properties, titles, chart colors, etc.
 */
const metric = {
    impressions: {
        prop: 'impressions',
        title: 'Impressions',
        format: numberFmt,
        round: round.whole,
        color: 'rgb(93, 165, 218)',
        fill: 'rgba(93, 165, 218, 0.2)',
        isGoodChange: (a, b) => a > b,
    },
    clicks: {
        prop: 'clicks',
        title: 'Clicks',
        format: numberFmt,
        round: round.whole,
        color: '#DECF3F',
        isGoodChange: (a, b) => a > b,
    },
    salesCount: {
        prop: 'salesCount',
        title: 'Sales (units)',
        format: numberFmt,
        round: round.whole,
        color: '#4D4D4D',
        dash: [15, 5],
        isGoodChange: (a, b) => a > b,
    },
    salesValue: {
        prop: 'salesValue',
        title: 'Sales ($)',
        format: moneyFmt,
        round: round.money,
        color: '#60BD68',
        isGoodChange: (a, b) => a > b,
    },
    sales: {
        prop: 'sales',
        title: 'Sales ($)',
        format: moneyFmt,
        round: round.money,
        color: '#60BD68',
        isGoodChange: (a, b) => a > b,
    },
    spend: {
        prop: 'spend',
        title: 'Spend',
        format: moneyFmt,
        round: round.money,
        color: '#F15854',
        dash: [6, 6],
        isGoodChange: (a, b) => a < b,
    },
    profit: {
        prop: 'profit',
        title: 'Profit',
        format: moneyFmt,
        round: round.money,
        color: 'green',
        isGoodChange: (a, b) => a > b,
    },
    acos: {
        prop: 'acos',
        title: 'ACOS',
        format: pctFmt,
        round: round.none,
        color: '#FAA43A',
        isGoodChange: (a, b) => a < b,
    },
    avgCpc: {
        prop: 'avgCpc',
        title: 'Average CPC',
        format: moneyFmt,
        round: round.money,
        color: '#B276B2',
        dash: [3, 3],
        isGoodChange: (a, b) => a < b,
    },
    ctr: {
        prop: 'ctr',
        title: 'Click-Through Rate',
        format: pctFmt,
        round: round.none,
        color: '#DECF3F',
        dash: [3, 3],
        isGoodChange: (a, b) => a > b,
    },
    knpeCount: {
        prop: 'knpeCount',
        title: 'KNP Estimate',
        format: numberFmt,
        round: round.whole,
        color: '#4D4D4D',
        dash: [10, 3, 3, 3],
        isGoodChange: (a, b) => a > b,
    },
    knpeValue: {
        prop: 'knpeValue',
        title: 'KU Income Estimate ($)',
        format: moneyFmt,
        round: round.money,
        color: '#60BD68',
        dash: [3, 3],
        isGoodChange: (a, b) => a > b,
    },
    knpeTotalValue: {
        prop: 'knpeTotalValue',
        title: 'Income Estimate w/ KU ($)',
        format: moneyFmt,
        round: round.money,
        color: '#60BD68',
        dash: [10, 3, 3, 3],
        isGoodChange: (a, b) => a > b,
    },
    knpeAcos: {
        prop: 'knpeAcos',
        title: 'ACOS w/ KU Estimate',
        format: pctFmt,
        round: round.none,
        color: '#FAA43A',
        dash: [10, 3, 3, 3],
        isGoodChange: (a, b) => a < b,
    },
};

module.exports = {
    metric,
    moneyFmt,
    pctFmt,
    numberFmt,
    roundFmt,
};
