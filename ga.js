const ua = require('universal-analytics');

module.exports = function(analyticsId, hostname) {
    const ga = ua(analyticsId);

    // This is next to useless, but at least we'll get *something*
    if (typeof window !== 'undefined') {
        window.onerror = merror;
    }

    function errorToObject(_error) {
        const err = {};
        Object.getOwnPropertyNames(_error).forEach(prop => {
            err[prop] = _error[prop];
        });
        return err;
    }

    function errorToString(_error) {
        return JSON.stringify(_error, (key, value) => {
            if (value instanceof Error) {
                return errorToObject(_error);
            }
            return value;
        });
    }

    function pageview(path) {
        ga.pageview(path).send();
    }

    function merror(...msg) {
        const ex = msg[0] instanceof Error ? msg[0] : new Error();

        // We do this to capture both the stack at the point which the error was
        // thrown (if any) and the stack where the error was caught. The latter
        // condition is more important in the cases where we don't actually capture
        // an exception but some other kind of message.
        const errstr = msg.map(errorToString).join(' ');
        const err = new Error(errstr);

        ga.exception(err.stack, !ex.handled).send();
        console.error(ex.handled ? '[handled]' : '', err);
    }

    /* Within the browser, we batch events together and only send them once every
     * 1s. This greatly reduces the cost of using events in hot paths.
     */
    let eventQueue = [];
    function mevent(category, action, label, value) {
        eventQueue.push([category, action, label, value]);
    }
    if (typeof window != 'undefined') {
        window.setInterval(() => {
            if (!eventQueue.length)
                return;

            let prevEvent = ga;
            for (const eventArgs of eventQueue) {
                prevEvent = prevEvent.event(...eventArgs);
            }
            prevEvent.send();
            eventQueue = [];
        }, 500);
    }

    function mclick(category, label) {
        ga.event(category, 'click', label).send();
    }

    function mcatch(fn) {
        return function(...args) {
            try {
                return fn.apply(this, args); // eslint-disable-line no-invalid-this
            }
            catch (ex) {
                merror(ex);
                ex.handled = true;
                throw ex;
            }
        };
    }

    function revent(eventId, eventData) {
        try {
            const opts = {
                method: 'PUT',
                headers: new window.Headers(),
                mode: 'cors',
                credentials: 'include',
                redirect: 'error',
                body: JSON.stringify({ eventId, eventData }),
            };
            opts.headers.set('Content-Type', 'application/json');

            window.fetch(`https://${hostname}/evt`, opts).then(response => {
                if (!response.ok && response.status != 401) {
                    merror(`revent ${eventId} ${eventData} response error: ${response.status} ${response.statusText}`);
                }
            })
            .catch(err => {
                merror(`revent ${eventId} ${eventData} response error: ${err}`);
            });
        }
        catch (ex) {
            merror(`revent ${eventId} ${eventData} create error`, ex);
        }
    }

    return {
        errorToObject, 
        errorToString, 
        mcatch,
        mclick,
        merror,
        mevent,
        pageview,
        revent,
    };
};
