# Machete Shared Components

This project contains code which is shared between the Machete browser plugin and
server-side components. These are mostly related to data transformation, data-gathering
logic, and communication with the server API.

This is a source-only repository intended to be included as a submodule within the other
repositories. As such, it doesn't contain package.json or any other module metadata.
Consumers of this repository should include it in an appropriate place and then import the
files it contains by path. Note that some of the modules here require certain other modules
to be installed. It's up to you to ensure that these are installed.

Files found here should not use es6 module syntax or include anything outside of this
module.
