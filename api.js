const { ajax, handleServerErrors } = require('./network');
const { cacheable } = require('./cache');
const { convertSnapshotsToDeltas, parallelQueue, pageArray, timestampSort } = require('./data-tools');

module.exports = function(serviceUrl) {
    const self = { // eslint-disable-line consistent-this
        getMissingDates(entityId) {
            return ajax(`${serviceUrl}/api/campaignData/${entityId}/missingDates`, {
                method: 'GET',
                responseType: 'json',
            });
        },

        getCampaignSummaries: cacheable(function(entityId) {
            return ajax(`${serviceUrl}/api/data/${entityId}/summary`, { 
                method: 'GET',
                responseType: 'json'
            });
        }, { name: 'campaignSummary', expireHours: 24, defaultValue: [] }),

        getEntityMetadata: cacheable(function() {
            return ajax(`${serviceUrl}/api/entities`, { 
                method: 'GET',
                responseType: 'json'
            });
        }, { name: 'entityMetadata', expireHours: 24, defaultValue: [] }),

        async storeEntityMetadata(entities) {
            const value = await ajax(`${serviceUrl}/api/entities`, {
                method: 'PUT',
                jsonData: entities,
            });
            await self.getEntityMetadata.clear();
            return value;
        },

        async releaseEntities(entities) {
            const value = await ajax(`${serviceUrl}/api/entities/release`, {
                method: 'POST',
                jsonData: entities,
            });
            await self.getEntityMetadata.clear();
            return value;
        },

        getAuthorEntities(entityId) {
            return ajax(`${serviceUrl}/api/entities/author/${entityId}`, {
                method: 'GET',
                responseType: 'json'
            });
        },

        storeAuthorEntities(entityId, authors) {
            return ajax(`${serviceUrl}/api/entities/author/${entityId}`, {
                method: 'PUT',
                jsonData: authors,
            });
        },

        releaseAuthorEntities(entityId, authors) {
            return ajax(`${serviceUrl}/api/entities/author/${entityId}/release`, {
                method: 'POST',
                jsonData: authors,
            });
        },

        getCampaignHistory: cacheable(async function({ entityId, campaignId }) {
            const snapshots = await ajax(`${serviceUrl}/api/data/${entityId}/${campaignId}`, { 
                method: 'GET',
                responseType: 'json'
            });
            const amsData = convertSnapshotsToDeltas(snapshots);

            /* TODO gotta fix this somehow
            if (await hasKdpIntegration()) {
                const summary = findSummary(await getCampaignSummaries(entityId), campaignId);
                if (summary && summary.asin) {
                    const kdpData = await getKdpSalesHistory(summary.asin);
                    return calculateKnpIncome(amsData, kdpData);
                }
            }
            */
            return amsData;
        }, { name: 'campaignHistory', expireHours: 12, defaultValue: [] }),

        getKeywordData: cacheable(function({ entityId, adGroupId }) {
            const url = `${serviceUrl}/api/keywordData/${entityId}/${adGroupId}`;
            const opts = {
                method: 'GET',
                responseType: 'json',
            };
            return ajax(url, opts);
        }, { name: 'keywordData', expireHours: 12, defaultValue: [] }),

        async patchKeywordData({ entityId, adGroupIds, keywordIds, timestamp, operation, dataValues }) {
            await parallelQueue(pageArray(keywordIds, 100), page =>
                ajax(`${serviceUrl}/api/keywordData/${entityId}?timestamp=${timestamp}`, {
                    method: 'PATCH',
                    jsonData: { operation, dataValues, keywordIds: page },
                    responseType: 'json',
                }));
            await self.getKeywordData.clear(args => args.entityId == entityId && adGroupIds.includes(args.adGroupId));
        },

        patchKeywordStatus({ entityId, adGroupIds, keywordIds, enable }) {
            return self.patchKeywordData({
                entityId,
                adGroupIds,
                keywordIds,
                timestamp: Date.now(),
                operation: enable ? "ENABLE" : "PAUSE",
                dataValues: {}
            });
        },

        patchKeywordBid({ entityId, adGroupIds, keywordIds, bid }) {
            return self.patchKeywordData({
                entityId,
                adGroupIds,
                keywordIds,
                timestamp: Date.now(),
                operation: 'UPDATE',
                dataValues: { bid }
            });
        },

        async storeLifetimeCampaignData(entityId, timestamp, data) {
            if (!(data && data.length))
                return;

            await ajax(`${serviceUrl}/api/data/${entityId}?timestamp=${timestamp}`, {
                method: 'PUT',
                jsonData: data,
                contentType: 'application/json',
            });
            await self.getCampaignSummaries.clear(entityId);
        },

        async storeDailyCampaignData(entityId, timestamp, data) {
            if (!(data && data.length))
                return;

            await ajax(`${serviceUrl}/api/campaignData/${entityId}?timestamp=${timestamp}`, {
                method: 'PUT',
                jsonData: data,
                contentType: 'application/json',
            });
            await self.getCampaignHistory.clear(args => args.entityId == entityId);
        },

        async getAggregateCampaignHistory({ entityId, campaignIds, range }) {
            let aggregate = [];
            const start = range.startDate.valueOf();
            const end = range.endDate.valueOf();
            await parallelQueue(campaignIds, async function(campaignId) {
                try {
                    const history = await self.getCampaignHistory({ entityId, campaignId });
                    aggregate = aggregate.concat(...history.filter(x => x.timestamp >= start && x.timestamp <= end));
                }
                catch (ex) {
                    if (handleServerErrors(ex) == 'notAllowed') {
                        // swallow this
                    }
                    throw ex;
                }
            });

            aggregate.sort(timestampSort);
            return aggregate;
        },

        getAggregateKeywordData({ entityId, adGroupIds }) {
            return parallelQueue(adGroupIds, function(adGroupId) {
                try {
                    return self.getKeywordData({ entityId, adGroupId });
                }
                catch (ex) {
                    if (handleServerErrors(ex) == 'notAllowed') {
                        // swallow this
                    }
                    throw ex;
                }
            });
        },

        async storeAdGroupMetadata(entityId, adGroupId, campaignId) {
            await ajax(`${serviceUrl}/api/adGroupMetadata/${entityId}/${adGroupId}`, {
                method: 'PUT',
                jsonData: { campaignId },
            });
            await self.getAdGroups.clear(args => args.entityId == entityId);
        },

        async storeKeywordData(entityId, adGroupId, timestamp, data) {
            // Chop the large keyword list into small, bite-sized chunks for easier
            // digestion on the server.
            await parallelQueue(pageArray(data, 100), chunk =>
                ajax(`${serviceUrl}/api/keywordData/${entityId}/${adGroupId}?timestamp=${timestamp}`, {
                    method: 'PUT',
                    jsonData: chunk,
                    contentType: 'application/json',
                }));
            await self.getKeywordData.clear({ entityId, adGroupId });
        },

        getAdGroups: cacheable(function(entityId) {
            return ajax(`${serviceUrl}/api/adGroups/${entityId}`, {
                method: 'GET',
                responseType: 'json',
            });
        }, { name: 'adGroups', expireHours: 6, defaultValue: [] }),

        async storeCampaignMetadata(entityId, campaignId, asin) {
            await ajax(`${serviceUrl}/api/campaignMetadata/${entityId}/${campaignId}`, {
                method: 'PUT',
                jsonData: { asin },
            });
            await self.getCampaignSummaries.clear(entityId);
        },

        async patchCampaignData(entityId, campaignIds, operation, dataValues) {
            const timestamp = Date.now();
            await ajax(`${serviceUrl}/api/campaignData/${entityId}`, {
                method: 'PATCH',
                queryData: { timestamp },
                jsonData: { campaignIds, operation, dataValues },
            });
            await self.getCampaignSummaries.clear(entityId);
        },

        getKdpSalesHistory: cacheable(function(asin) {
            return ajax(`${serviceUrl}/api/kdp/${asin}/history`, {
                method: 'GET',
                responseType: 'json'
            });
        }, { name: 'kdpSalesHistory', expireHours: 6, defaultValue: [] }),

        async storeKdpData(asin, sales, ku) {
            await ajax(`${serviceUrl}/api/kdp/${asin}/history`, {
                method: 'PUT',
                jsonData: { sales, ku },
            });
            await self.getKdpSalesHistory.clear(asin);
        },

        getPortfolios: cacheable(function (entityId) {
            return ajax(`${serviceUrl}/api/portfolios/${entityId}`, {
                method: 'GET',
                responseType: 'json',
            });
        }, { name: 'portfolios', expireHours: 12, defaultValue: [] }),

        async storePortfolios(entityId, portfolios) {
            await ajax(`${serviceUrl}/api/portfolios/${entityId}`, {
                method: 'PUT',
                jsonData: portfolios,
            });
            await self.getPortfolios.clear(entityId);
        },
    };
    return self;
};
