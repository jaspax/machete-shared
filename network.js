const sleep = require('sleep-promise').default;

function buildFetchOptions(url, opts) {
    const init = Object.assign({
        method: 'GET',
        headers: new window.Headers(),
        mode: 'cors',
        credentials: 'include',
    }, opts);

    if (opts.queryData) {
        const query = new window.URLSearchParams();
        for (const key of Object.keys(opts.queryData)) {
            query.append(key, opts.queryData[key]);
        }
        url += '?' + query.toString();
    }

    if (opts.formData && opts.jsonData)
        throw new Error('Cannot set both formData and jsonData on ajax request');

    if (opts.formData) {
        init.body = new window.URLSearchParams();
        for (const key of Object.keys(opts.formData)) {
            init.body.append(key, opts.formData[key]);
        }
        init.headers.set('Content-Type', 'application/x-www-form-urlencoded');
    }
    else if (opts.jsonData) {
        init.body = JSON.stringify(opts.jsonData);
        init.headers.set('Content-Type', 'application/json');
    }

    return { url, opts: init };
}

async function shouldRetry(response) {
    if (response.status == 429) { // 429 Too Many Requests
        return true;
    }
    if (response.status == 403) { // 403 Access too frequently!
        const text = await response.text();
        return text.toLowerCase().includes("frequently");
    }
    return false;
}

module.exports = {
    async ajax(origUrl, origOpts = {}) {
        const { url, opts } = buildFetchOptions(origUrl, origOpts);
        const origStack = new Error().stack;
        while (true) { // eslint-disable-line no-constant-condition
            try {
                const response = await window.fetch(url, opts);
                if (!response.ok) {
                    if (await shouldRetry(response)) {
                        await sleep(5000);
                        continue;
                    }

                    throw new Error(`${response.status} ${response.statusText}`);
                }
                if (response.redirected) {
                    // this is USUALLY because we got redirected to a login page. In
                    // this case we fake out a 401 so that calling code handles it
                    // correctly.
                    throw new Error(`401 Redirect to ${response.url}`);
                }

                if (response.status == 204)
                    return null;

                const body = await response.text();
                if (opts.responseType == 'json') {
                    if (!body.length)
                        return {};
                    return JSON.parse(body);
                }
                return body;
            }
            catch (ex) {
                ex.method = opts.method;
                ex.url = url;
                ex.origStack = origStack;
                if (window && window.onAjaxError && !window.onAjaxError(ex))
                    return [];
                throw ex;
            }
        }
    },

    handleServerErrors(ex, desc) {
        console.log(ex, desc);
        if (ex.url && (ex.url.startsWith('/') || ex.url.match(/machete-app.com/))) {
            if (ex.message.match(/^401/)) {
                return 'notLoggedIn';
            }
            if (ex.message.match(/^402/)) {
                return 'notAllowed';
            }
            if (ex.message.match(/^403/)) {
                return 'notOwned';
            }
        }
        if (ex.message.match(/^401/)) {
            return 'amazonNotLoggedIn';
        }
        if (ex.message.match(/^403/)) {
            return 'amazonNotLoggedIn';
        }
        if (ex.message.match(/^404/)) {
            return 'notFound';
        }
        if (ex.message.match(/^50/)) {
            return 'serverError';
        }
        if (ex.message.match(/0 error/) || ex.message.match(/Failed to fetch/)) {
            return 'amazonNotLoggedIn';
        }
        return null;
    }
};
