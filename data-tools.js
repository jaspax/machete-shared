const _ = require('lodash');
const fmt = require('./fmt.js');
const moment = require('moment');
const qu = require('async/queue');
const qw = require('qw');

const cumulativeMetrics = qw`impressions clicks salesCount salesValue spend knpeCount knpeValue knpeTotalValue`;
const cumulativeKeywordMetrics = qw`impressions clicks sales spend`;

function stripPrefix(id) {
    if (!id)
        return id;
    if (!id.replace)
        return id;
    return id.replace(/^AX?/, '');
} 

const statusStrings = {
    enabled: "Enabled",
    ended: "Ended",
    running: "Running",
    scheduled: "Scheduled",
    asin_not_buyable: "ASIN Not Buyable", // eslint-disable-line camelcase
    billing_error: "Billing Error", // eslint-disable-line camelcase
    'billing error': "Billing Error",
    cp_campaign_ineligible: "Campaign Ineligible", // eslint-disable-line camelcase
    landing_page_not_available: "Landing Page Unavailable", // eslint-disable-line camelcase
    paused: "Paused",
    pendingreview: "Pending Review",
    processing: "Processing",
    rejected: "Rejected",
    terminated: "Terminated",
};

function lc(str) {
    return (str || '').toLowerCase();
}

function statusText(summary) {
    const state = lc(_.get(summary, 'state'));
    if (state == 'created')
        return 'Created';
    if (state == 'archived')
        return 'Archived';
    return statusStrings[lc(_.get(summary, 'status'))] || 'Unknown status';
}

function isPausable(summary) {
    return ["enabled", "running", "scheduled"].includes(lc(_.get(summary, 'status'))) && !isEnded(summary);
}

function isEnded(summary) {
    return lc(_.get(summary, 'status')) == 'ended' || lc(_.get(summary, 'state')) == 'archived' || moment().isAfter(moment(_.get(summary, 'endDate')));
}

function isEnabled(summary) {
    return lc(_.get(summary, 'state')) == "enabled" && !isEnded(summary);
}

function isArchived(summary) {
    return lc(_.get(summary, 'state')) == 'archived';
}

function isAuthorEntity(entityId) {
    if (!entityId)
        return null;
    if (entityId.startsWith('AUTHOR:'))
        return entityId.substring(7);
    return null;
}

function findSummary(summaries, campaignId) {
    const summary = (summaries || []).find(x => x.campaignId == campaignId);
    return summary || { campaignId };
}

function findKeyword(keywords, kwid) {
    const keyword = (keywords || []).find(x => x.id == kwid || (x.id && x.id.includes(kwid)));
    return keyword || { id: kwid };
}

function calculateKnpIncome(amsSales, kdpSales) {
    if (!kdpSales)
        return amsSales;

    // Copy and ensure these are sorted
    amsSales = [...amsSales].sort(timestampSort);
    kdpSales = [...kdpSales].sort(timestampSort);

    // These buffers hold the most recent 15 records from each source
    const kdpWindow = [];
    const amsWindow = [];
    const pushWindow = (a, item) => {
        a.push(item);
        if (a.length > 15)
            a.shift();
    };

    const rv = [];
    let kdpItem = kdpSales.shift();
    for (const item of amsSales) {
        const newItem = Object.assign({}, item);
        if (kdpItem) {
            // get kdpSales aligned with the current amsSales item
            while (kdpItem.timestamp < item.timestamp) {
                pushWindow(kdpWindow, kdpItem);
                kdpItem = kdpSales.shift();
            }
            const kdpSalesCount = kdpWindow.reduce((sum, kdp) => sum + kdp.paidEbook + kdp.paidPaperback, 0);
            const amsSalesCount = amsWindow.reduce((sum, ams) => sum + (ams.salesCount || 0), 0);
            const ratio = kdpSalesCount ? amsSalesCount / kdpSalesCount : 0;

            const sales = item.salesValue || item.sales || 0; // salesValue for campaigns, sales for keywords
            newItem.knpeCount = ((kdpItem && kdpItem.knp) || 0) * ratio;
            newItem.knpeValue = newItem.knpeCount * 0.005;
            newItem.knpeTotalValue = sales + newItem.knpeValue;
            newItem.knpeAcos = newItem.knpeTotalSales ? 100 * (newItem.spend / newItem.knpeTotalSales) : null;
        }
        else {
            newItem.knpeCount = null;
            newItem.knpeValue = null;
            newItem.knpeTotalValue = null;
            newItem.knpeAcos = null;
        }
        rv.push(newItem);

        // we're done with this item, so push it into the window
        pushWindow(amsWindow, item);
    }

    return rv;
}

const defaultOptimizeOpts = {
    minImpressions: 1000,
    minClicks: 0,
    minSales: 0,
};

function timestampSort(a, b) {
    return a.timestamp - b.timestamp;
}

function filterKw(kw, opts, optimize) {
    kw = Object.assign({}, kw);
    if (kw.impressions < opts.minImpressions) {
        kw.optimizeResult = 'lowImpressions';
    }
    else if (kw.clicks < opts.minClicks) {
        kw.optimizeResult = 'lowClicks';
    }
    else if (kw.sales < opts.minSales) {
        kw.optimizeResult = 'lowSales';
    }
    else {
        kw.optimizeResult = 'optimized';
        optimize(kw);
    }
    return kw;
}

function boundRatiox2(ratio, maxRatio = 2) {
    return Math.max(0.5, Math.min(2, maxRatio, ratio));
}

function boundBidMinMax(bid, opts) {
    if (opts.minBid && bid < opts.minBid)
        return opts.minBid;
    if (opts.maxBid && bid > opts.maxBid)
        return opts.maxBid;
    return bid.toFixed(2);
}

function optimizeKeywordsAcos(targetAcos, kws, opts = defaultOptimizeOpts) {
    return kws.map(x => filterKw(x, opts, kw => {
        const ratio = boundRatiox2(targetAcos / kw.acos);
        if (kw.avgCpc)
            kw.optimizedBid = boundBidMinMax(kw.avgCpc * ratio, opts);
    }));
}

function optimizeKeywordsSalesPerDay(targetSalesPerDay, campaignSeries, kws, opts = defaultOptimizeOpts) {
    const now = moment();
    const earliest = Math.min(...campaignSeries.map(x => x.timestamp));
    const campaignLifetime = sumCampaignSeries(campaignSeries);
    const campaignDays = now.diff(moment(earliest), 'days');
    const campaignSalesPerDay = campaignLifetime.salesValue / campaignDays;
    const campaignSalesPerClick = campaignLifetime.salesValue / campaignLifetime.clicks;
    const ratio = targetSalesPerDay / campaignSalesPerDay;

    return kws.map(x => filterKw(x, opts, kw => {
        const kwSalesPerClick = kw.sales / kw.clicks;

        // constrain ratios to a 2x change in either direction to avoid wild swings
        const finalRatio = boundRatiox2(ratio * (kwSalesPerClick / campaignSalesPerClick));
        if (kw.avgCpc)
            kw.optimizedBid = boundBidMinMax(kw.avgCpc * finalRatio, opts);
    }));
}

// Calculate the statistical measures for a delta at a particular time
function calculateItemStats(item) {
    const sales = Number(item.salesValue || item.sales) || 0; // salesValue for campaigns, sales for keywords
    item.profit = sales - (Number(item.spend) || 0);
    item.acos = sales ? 100 * (item.spend / sales) : null;
    item.avgCpc = item.spend / item.clicks;
    item.ctr = item.impressions ? 100 * (item.clicks / item.impressions) : null;
    item.knpeAcos = item.knpeTotalSales ? 100 * (item.spend / item.knpeTotalSales) : null;
    return item;
}

// Convert a series of irregularly spaced deltas into a series of evenly-spaced
// deltas with the spacing given by 'chunk'. Chunk may be any timespan value
// recognized by moment.js.
function chunkSeries(data, chunk) {
    if (!chunk)
        throw new Error("parameter 'chunk' is required");

    const result = [];
    let lastItem = null;
    let lastOrigItem = null;
    for (const origItem of data.filter(x => x)) {
        const item = Object.assign({}, origItem);
        item.timestamp = moment.tz(item.timestamp, 'UTC').startOf(chunk).valueOf();

        // Sanity check
        if (lastOrigItem && origItem.timestamp < lastOrigItem.timestamp) {
            throw new Error('arrays passed to chunkSeries must be sorted by timestamp');
        }

        if (!lastItem) {
            lastItem = item;
        }
        else if (item.timestamp == lastItem.timestamp) {
            for (const metric of cumulativeMetrics) {
                lastItem[metric] += item[metric] || 0;
            }
        }
        else {
            if (origItem.timestamp != item.timestamp) {
                // When crossing a chunk boundary with irregularly spaced data,
                // calculate the portion of the time diff that lies within the
                // chunk we're entering and the chunk we're leaving, and then
                // credit each item with the proper proportion of the change
                const span = origItem.timestamp - lastOrigItem.timestamp;
                const thisChunkRatio = (origItem.timestamp - item.timestamp) / span;
                const lastChunkRatio = (moment(lastItem.timestamp).endOf(chunk).valueOf() - lastOrigItem.timestamp) / span;
                for (const metric of cumulativeMetrics) {
                    const metricConfig = fmt.metric[metric];
                    lastItem[metric] += metricConfig.round(item[metric] * lastChunkRatio);
                    item[metric] = metricConfig.round(item[metric] * thisChunkRatio);
                }
            }

            calculateItemStats(lastItem);
            result.push(lastItem);
            lastItem = item;
        }

        lastOrigItem = origItem;
    }

    if (lastItem) {
        calculateItemStats(lastItem);
        result.push(lastItem);
    }

    return result;
}

// Convert a series of mixed snapshots and deltas into a series of objects in
// which each key has the difference from the previous snapshot, ie. the rate of
// change between snapshots. Only the metrics found in `cumulativeMetrics` are
// converted into rates. The opt object has the following relevant keys:
//      startTimestamp: earliest timestamp to examine. Items outside of this
//          range are discarded.
//      endTimestamp: latest timestamp to examine. Items outside of this range
//          are discarded.
function convertSnapshotsToDeltas(data, opt) {
    const deltas = [];
    opt = opt || {};

    let lastSnapshot = null;
    let prevDecreased = false;
    data = data.sort(timestampSort);
    for (const item of data) {
        // Filter out things by date range
        if (opt.startTimestamp && item.timestamp < opt.startTimestamp) {
            continue;
        }
        if (opt.endTimestamp && item.timestamp > opt.endTimestamp) {
            continue;
        }

        // identical timestamps are probably server-side duplicates
        if (lastSnapshot && item.timestamp == lastSnapshot.timestamp) {
            continue;
        }

        if (item.measurementType == 'daily') {
            deltas.push(Object.assign({}, item));
            if (lastSnapshot) {
                for (const metric of cumulativeMetrics) {
                    lastSnapshot[metric] += item[metric];
                }
                lastSnapshot.timestamp = item.timestamp;
                prevDecreased = false;
            }
        }
        else { // measurementType == 'snapshot'
            if (lastSnapshot) {
                // Skip this if any metric decreased, but don't skip multiple rows
                if (!prevDecreased && cumulativeMetrics.some(metric => item[metric] < lastSnapshot[metric])) { // eslint-disable-line no-loop-func
                    prevDecreased = true;
                    continue;
                }
                prevDecreased = false;

                const delta = Object.assign({}, item);
                for (const metric of cumulativeMetrics) {
                    delta[metric] = item[metric] - lastSnapshot[metric];
                }
                deltas.push(delta);
            }

            lastSnapshot = item;
        }
    }

    return deltas;
}

function aggregateSeries(series, opt = { chunk: 'day' }) {
    const a = {};
    for (const thisSeries of series) {
        for (const item of thisSeries) {
            const timestamp = moment(item.timestamp).startOf(opt.chunk).valueOf();
            if (a[timestamp]) {
                for (const key of cumulativeMetrics) {
                    a[timestamp][key] = (item[key] || 0) + (a[timestamp][key] || 0);
                }
            }
            else {
                a[timestamp] = Object.assign({}, item);
            }
        }
    }

    const agg = _.keys(a).sort().map(x => a[x]);
    agg.forEach(calculateItemStats);
    return agg;
}

function sumCampaignSeries(series) {
    const sum = {};
    for (const item of series) {
        for (const key of Object.keys(item)) {
            if (cumulativeMetrics.includes(key)) {
                sum[key] = ((item && item[key]) || 0) + (sum[key] || 0);
            }
            else {
                sum[key] = item[key];
            }
        }
    }
    calculateItemStats(sum);
    return sum;
}

function aggregateKeywords(kwSets) {
    // Aggregate the cumulative metrics
    const a = {};
    for (const kws of kwSets) {
        for (const item of kws) {
            const kw = item.keyword;
            if (a[kw]) {
                for (const key of cumulativeKeywordMetrics) {
                    a[kw][key] = item[key] + (a[kw][key] || 0);
                }
                a[kw].id.push(item.id);
                a[kw].adGroupId.push(item.adGroupId);
                a[kw].bid = Math.max(a[kw].bid, item.bid);
                a[kw].enabled = a[kw].enabled || item.enabled;
            }
            else {
                a[kw] = Object.assign({}, item);
                a[kw].id = [item.id];
                a[kw].adGroupId = [item.adGroupId];
            }
        }
    }

    // Recalculate the aggregate metrics
    const keywords = _.values(a);
    keywords.forEach(calculateItemStats);
    return keywords;
}

function sumKeywordSeries(data) {
    const keywords = {};
    for (const record of data.sort((a, b) => a.timestamp - b.timestamp)) {
        const kw = record.keyword;
        if (!keywords[kw])
            keywords[kw] = {};
        _.each(_.keys(record), key => {
            if (cumulativeKeywordMetrics.includes(key)) {
                if (isNaN(keywords[kw][key]))
                    keywords[kw][key] = 0;
                keywords[kw][key] += record[key];
            }
            else {
                keywords[kw][key] = record[key];
            }
        });
    }

    const values = _.values(keywords);
    values.forEach(calculateItemStats);

    return values;
}

function numberize(str) {
    if (!str)
        return str;
    if (typeof str == 'number')
        return str;
    if (str.hasOwnProperty('millicents')) // eslint-disable-line no-prototype-builtins
        return numberize(str.millicents) / 100000;
    str = str.replace(/,/g, '');
    str = str.replace(/\s/g, '');
    return parseFloat(str);
}

function diffCampaignSnapshots(a, b) {
    const diff = {};
    for (const key of cumulativeMetrics) {
        diff[key] = (a[key] || 0) - (b[key] || 0);
    }
    return diff;
}

function parallelQueue(items, fn, queueWidth = 5) {
    return new Promise((resolve, reject) => {
        const results = [];
        let error = null;
        const queue = qu((item, callback) => {
            fn(item).then(value => {
                results.push(value);
                callback(null, value);
            })
            .catch(ex => {
                error = ex;
                callback();
            });
        }, queueWidth);

        queue.drain = () => {
            if (error)
                reject(error);
            else
                resolve(results);
        };
        queue.error = reject; // shouldn't happen since we're swallowing errors until drain
        queue.push(Array.from(items));
    });
}

function* pageArray(array, step) {
    if (!array || !array.length)
        return;
    for (let index = 0; index < array.length; index += step) {
        yield array.slice(index, index + step);
    }
}

module.exports = {
    aggregateKeywords,
    aggregateSeries,
    calculateKnpIncome,
    chunkSeries,
    convertSnapshotsToDeltas,
    diffCampaignSnapshots,
    findKeyword,
    findSummary,
    isArchived,
    isAuthorEntity,
    isEnabled,
    isEnded,
    isPausable,
    numberize,
    optimizeKeywordsAcos,
    optimizeKeywordsSalesPerDay,
    pageArray,
    parallelQueue,
    statusText,
    stripPrefix,
    sumCampaignSeries,
    sumKeywordSeries,
};
